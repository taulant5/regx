#ifndef REGX_AUTMATONHELPERCLASSES_HPP
#define REGX_AUTMATONHELPERCLASSES_HPP
#include <vector>

/*Basic read only structure to represent a tuple*/
template <class firstPart, class secondPart>
struct tuple
{
    const firstPart first;
    const secondPart second;
    tuple() = delete;
    tuple(const firstPart& _first, const secondPart& _second):first(_first), second(_second){}
};

typedef const tuple<const unsigned int&, const char&> automatonInput;
typedef const tuple<const unsigned int&, const unsigned int&> statePair;





unsigned int addVectorWithoutDuplicate(std::vector<unsigned int>& vec, const std::vector<unsigned int>& sum);
bool addWithoutDuplicate(std::vector<unsigned int>& vec, const unsigned int& sum);
std::vector<unsigned int> difference(const std::vector<unsigned int>& origin, const std::vector<unsigned int>& toBeChecked);//Finds differences between origin and toBeChecked
//outputs what's missing in toBeChecked to have origin

bool isElementInVector(const std::vector<unsigned int>& vec, const unsigned int& value);


#endif //REGX_AUTMATAHELPERCLASSES_HPP
