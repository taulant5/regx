#ifndef REGX_DFA_AUTOMATON_HPP
#define REGX_DFA_AUTOMATON_HPP
#include <vector>
#include <string>
#include "AutomatonHelper.hpp"








class dfa_automaton {

private:

    std::vector<char> m_alphabet;           //Represents the alphabet. Note that there won't be duplicates
    unsigned int m_states;                  //Represents the different states, where each state is given a number, 0 represents the start state
    std::vector<unsigned int> m_endStates;
    std::vector<unsigned int> m_rules;
public:

    //default constructor is deleted
    dfa_automaton() = delete;


    dfa_automaton(const dfa_automaton&);

    //Adds the alphabet and the total number of states to the automat, note that same letters will be skipped
    dfa_automaton(const std::string& alphabet, const unsigned int& states);

    //Add a rule to the automaton
    void setRule(const automatonInput&, const unsigned int&);

    //Adds endState, a word is legit, if it has reached any endstate at the end
    void addEndState(const unsigned int&);

    //returns the state when a tuple is given, if the rule is not defined throws exception
    const unsigned int& getState(const automatonInput&) const;


    bool isInLanguage(const std::string&, unsigned int startState = 0) const;

    //returns true when every possibe autmatInput tuple is defined
    bool isDefined() const;

    //returns true when given state is one of the endstates
    bool isEndState(const unsigned int&) const;

    //returns then number of states
    const unsigned int& getStateCount() const;

    //uses findMergable() and merge() to make the minimal automata
    void makeMinimal();

    //makes the complement of given Language, meaning all the words that are not in the language, are part of the language
    //and all words that were part of the language are no longer
    void makeComplement();

    //merges two states by overwriting the rules and making every rule pointing to the smaller state.
    //note that the structure is overwritten and rules might not be the same as earlier
    //so when you want to add something make sure you've checked with getStateCount() and getState() what valid rules are
    //CAREFUL WHEN MERGING as the library won't check whether the pair is valid or not also mind the given list in findMergable doesn't update
    //so you might want to use the function each time you have merged
    void merge(const statePair&);

    //finds mergable states and returns them in a tuple inside a vector
    std::vector<tuple<unsigned int, unsigned int>> findMergable() const;
};


#endif
