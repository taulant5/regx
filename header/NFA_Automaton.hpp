#ifndef REGX_NFA_AUTOMATON_HPP
#define REGX_NFA_AUTOMATON_HPP
#include "AutomatonHelper.hpp"
#include <vector>
#include <string>
#include <iostream>
class nfa_automaton {

private:

    std::vector<char> m_alphabet;           //Represents the alphabet. Note that there won't be duplicates
    unsigned int m_states;                  //Represents the different states, where each state is given a number doesn't have a default begin state
    std::vector<unsigned int> m_endStates;
    std::vector<unsigned int> m_startStates;
    std::vector<std::vector<unsigned int>> m_rules; //Represents the different rules, since one can move over different states we need a vector for that


public:

    nfa_automaton() = delete;

    nfa_automaton(const nfa_automaton&);

    nfa_automaton(const std::string& alphabet, const unsigned int& states);

    void setRule(const automatonInput&, const unsigned int&);

    void addEndState(const unsigned int&);

    const std::vector<unsigned int>& getState(const automatonInput&) const;

    std::vector<unsigned int> getStates(const std::vector<unsigned int>&, const char&) const;

    const std::vector<unsigned int>& getStartStates() const;

    const std::vector<unsigned int>& getEndStates() const;

    void addStartState(const unsigned int&);

    bool isInStates(const std::vector<unsigned int>&) const;

    bool isInLanguage(const std::string&) const;

    bool isInLanguage(const std::string&, std::vector<unsigned int> startStates) const;

    bool isEndState(const unsigned int&) const;

    bool isStartState(const unsigned int&) const;

    void resetEndState();

    void resetStartState();

    const unsigned int& getStateCount() const;

    std::vector<unsigned int> findReachableStates(std::vector<unsigned int> startState) const; //Note, that every rule pointing towards this state will be removed, since we are iterating through the each and every rule

    std::vector<unsigned int> findUnreachableStates() const;    //this makes removing a heavy operation

    void removeState(const unsigned int& state);


void Status()
{
    std::cout << "States:" << m_states << std::endl;
    std::cout << "Startstates:";
    for(int i = 0; i < m_startStates.size(); i++)
        std::cout << " " << m_startStates[i];
    std::cout << std::endl << "EndStates:";
    for(int i = 0; i < m_endStates.size(); i++)
        std::cout << " " << m_endStates[i];
    std::cout << std::endl;
    std::cout << "Rules:" << std::endl;
    for(int i = 0; i < m_states; i++)
    {
        std::cout << "State " << i << ":";
        for(int y = 0; y < m_alphabet.size(); y++) {
            std::cout << " " << m_alphabet[y] << ":";
            for (int x = 0; x < m_rules[i * m_alphabet.size() + y].size(); x++)
                std::cout << " " << m_rules[i*m_alphabet.size() + y][x];
            std::cout << " |";
        }

        std::cout << std::endl;
    }


}

};



#endif
