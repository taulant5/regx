#ifndef REGX_REGX_HPP
#define REGX_REGX_HPP
#include "epsilon_NFA_Automaton.hpp"

class RegX
{
private:
    std::string m_alphabet;

public:
    explicit RegX(const std::string& alphabet):m_alphabet(alphabet)
    {

    }


    e_nfa_automaton letterAu(const char& letter)
    {
        e_nfa_automaton temp(m_alphabet, 2);
        temp.addStartState(0);
        temp.addEndState(1);
        temp.setRule(automatonInput(0,letter), 1);
        return temp;
    }

    e_nfa_automaton epsilonAu()
    {
        e_nfa_automaton temp(m_alphabet, 1);
        temp.addStartState(0);
        temp.addEndState(0);
        return temp;
    }

};









#endif //REGX_REGX_HPP
