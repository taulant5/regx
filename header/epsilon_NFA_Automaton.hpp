#ifndef REGX_EPSILON_NFA_AUTOMATA_HPP
#define REGX_EPSILON_NFA_AUTOMATA_HPP
#include <vector>
#include <string>
#include "NFA_Automaton.hpp"


//This class has storage functionality only as it can be converted easily into a nea without epsilon
//The BIG difference though is that this class can easily implement concatenation, star and union operators, since it supports epsilon rules
class e_nfa_automaton
{
public:
    std::string m_alphabet;
    unsigned int m_states;
    std::vector<std::vector<unsigned int>> m_rules;
    std::vector<std::vector<unsigned int>> m_epsilonRules;
    std::vector<unsigned int> m_startStates;
    std::vector<unsigned int> m_endStates;




public:

    e_nfa_automaton(const e_nfa_automaton& a);


    explicit e_nfa_automaton(const std::string& alphabet, const unsigned int& states = 0);


    e_nfa_automaton& Concatenate(const e_nfa_automaton& a);


    e_nfa_automaton& Union(const e_nfa_automaton& a);


    e_nfa_automaton& Star();


    void addStates(const unsigned int& i = 1);


    void setRule(const automatonInput& input, const unsigned int& outState);


    void epsilonRule(const unsigned int& input, const unsigned int& output);


    void addEndState(const unsigned int& state);


    void addStartState(const unsigned int& state);


    bool isEndState(const unsigned int& state) const;


    bool isEndStates(const std::vector<unsigned int>&input ) const;


    bool isStartState(const unsigned int& state)const;


    std::vector<unsigned int> findEpsilonRules(const unsigned int& input) const;


    nfa_automaton generateNEA() const;

};


#endif //REGX_EPSILON_NEA_AUTOMATA_HPP
