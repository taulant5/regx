#include "../header/AutomatonHelper.hpp"

unsigned int addVectorWithoutDuplicate(std::vector<unsigned int>& vec, const std::vector<unsigned int>& sum)
{
    unsigned int count = 0;
    for(unsigned int i = 0; i < sum.size(); i++)
    {
        bool alreadyExists = false;
        for(int j = 0; j < vec.size(); j++)
        {
            if(vec[j] == sum[i])
            {
                alreadyExists = true;
                break;
            }

        }

        if(!alreadyExists) {
            vec.push_back(sum[i]);
            count++;
        }
    }
    return count;
}



bool addWithoutDuplicate(std::vector<unsigned int>& vec, const unsigned int& sum)
{
    for(int i = 0; i < vec.size(); i++)
        if(vec[i] == sum)
            return false;

    vec.push_back(sum);
    return true;

}



std::vector<unsigned int> difference(const std::vector<unsigned int>& origin, const std::vector<unsigned int>& toBeChecked)
{
    std::vector<unsigned int> temp;

    for(unsigned int i = 0; i < origin.size(); i++) {
        bool isAlreadyIn = false;
        for (unsigned int y = 0; y < toBeChecked.size(); y++) {
            if (origin[i] == toBeChecked[y]){
                isAlreadyIn = true;
                break;
            }
        }
        if(!isAlreadyIn)
            temp.push_back(origin[i]);
    }

    return temp;
}




bool isElementInVector(const std::vector<unsigned int>& vec, const unsigned int& value)
{
    for(int i = 0; i < vec.size();i++)
        if(vec[i] == value)
            return true;


    return false;

}
