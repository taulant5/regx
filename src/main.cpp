#include <iostream>
#include "../header/DFA_Automaton.hpp"
#include "../header/RegX.hpp"

int main()
{

    RegX rex("a");
    try {

        //(a?)^n(a)^n against a^n
        unsigned int n = 29;

        std::string testString;
        for(int i = 0; i < n; i++)
            testString +='a';


        e_nfa_automaton test2 = rex.letterAu('a').Union(rex.epsilonAu());
        for(int i = 0 ; i < n - 1; i++)
            test2.Concatenate(rex.letterAu('a').Union(rex.epsilonAu()));
        for(int i = 0; i < n; i++)
            test2.Concatenate(rex.letterAu('a'));

        nfa_automaton test = test2.generateNEA();

        std::vector<unsigned int> unreachable = test.findUnreachableStates();
        while(!unreachable.empty())
        {
            test.removeState(unreachable.back()); //When always removing the biggest state possible the intern structure wont be affected, so you
            unreachable.pop_back();                 //keep removing
        }





        test.Status();
        std::cout << std::endl << std::endl << testString << " is in language:" << test.isInLanguage(testString);
    }catch(const char* e)
    {
        std::cout << e;
    }



}


















/*int main()
{
    dea_automata d("ab", 6);
    d.addEndState(2);
    d.setRule(automataInput(0,'a'), 1);
    d.setRule(automataInput(0,'b'), 3);
    d.setRule(automataInput(1,'a'), 1);
    d.setRule(automataInput(1,'b'), 2);
    d.setRule(automataInput(2,'a'), 2);
    d.setRule(automataInput(2,'b'), 2);
    d.setRule(automataInput(3,'a'), 3);
    d.setRule(automataInput(3,'b'), 4);
    d.setRule(automataInput(4,'a'), 5);
    d.setRule(automataInput(4,'b'), 4);
    d.setRule(automataInput(5,'a'), 5);
    d.setRule(automataInput(5,'b'), 2);
    //d.getStatus();
    d.makeMinimal();
    //d.getStatus();


}
*/
/*int main()
{
    e_nea_automata e("ab",1);
    e.addState();
    e.epsilonRule(0,1);
    e.addState();
    e.addState();
    e.addState();
    e.epsilonRule(3,4);
    e.addStartState(0);
    e.addEndState(1);
    e.addEndState(2);
    e.addEndState(4);


    e.setRule(automataInput(0,'b'), 2);
    e.setRule(automataInput(0,'a'), 3);
    e.setRule(automataInput(3,'b'), 4);
    e.setRule(automataInput(1,'a'), 4);

    nea_automata n = e.generateNEA();
    std::vector<unsigned int> a;
    n.findUnreachableStates().swap(a);
    while(!a.empty())
    {
        n.removeState(a.back());
        n.findUnreachableStates().swap(a);
    }
    n.getStatus();
    std::cout << std::endl;
    std::cout << n.isInLanguage("ab");  //yes
    std::cout << n.isInLanguage("aba"); //no


}*/
