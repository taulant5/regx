#include "../header/NFA_Automaton.hpp"

nfa_automaton::nfa_automaton(const std::string& alphabet, const unsigned int& states):m_states(states) {

    for(unsigned int pos = 0; pos < alphabet.length(); pos++)
    {
        bool alreadyExists = false;
        for(int i = 0; i < m_alphabet.size(); i++)
        {
            if(alphabet[pos] == m_alphabet[i])
            {
                alreadyExists = true;
                break;
            }

        }

        if(!alreadyExists)
            m_alphabet.push_back(alphabet[pos]);
    }

    m_rules.resize(m_states*m_alphabet.size());    //each tuple(state,letter) has it's own vector of possible outcomes
}



void nfa_automaton::setRule(const automatonInput& input, const unsigned int& outState) {

    if(input.first >= m_states)
        throw "Unknown startState given as parameter while setting Rule";
    if(outState >= m_states)
        throw "Unknown endState given as parameter while setting Rule";



    for(int pos = 0; pos < m_alphabet.size(); pos++)
    {
        if(m_alphabet[pos] == input.second)
        {
            for(int i = 0; i < m_rules[input.first*m_alphabet.size() + pos].size(); i++)
            {
                if(m_rules[input.first*m_alphabet.size() + pos][i] == outState)                 //Since we can have multiple outcomes we first check whether there
                    return;                                                                     //already is
            }
            m_rules[input.first*m_alphabet.size() + pos].push_back(outState);

            return;
        }
    }

    throw "unknown character";                                              //If you came this far, it's either and unkown state
                                                                            //or unknown character but the state has been checked earlier
}



void nfa_automaton::addEndState(const unsigned int& state) {

    if(state >= m_states)
        throw "unknown state to be marked as endState";
    addWithoutDuplicate(m_endStates, state);
}



const std::vector<unsigned int>& nfa_automaton::getState(const automatonInput& input)const{

    if(input.first >= m_states)
        throw "unknown state, while getting State";


    for(int pos = 0; pos < m_alphabet.size(); pos++)
    {
        if(m_alphabet[pos] == input.second)
        {
            return m_rules[input.first*m_alphabet.size() + pos];
        }
    }

    throw "unknown character";
}



std::vector<unsigned int> nfa_automaton::getStates(const std::vector<unsigned int>& inputState, const char& letter)const {
    std::vector <unsigned int> temp;

    for(int i = 0; i < inputState.size(); i++)
        addVectorWithoutDuplicate(temp, getState(automatonInput(inputState[i], letter)));
    return temp;
}



bool nfa_automaton::isInStates(const std::vector<unsigned int>& toBeChecked) const {
    for(int i = 0; i < toBeChecked.size(); i++)
        if(toBeChecked[i] >= m_states)
            return false;

    return true;
}



nfa_automaton::nfa_automaton(const nfa_automaton& a) {
    this->m_endStates = a.m_endStates;
    this->m_startStates = a.m_startStates;
    this->m_alphabet = a.m_alphabet;
    this->m_states = a.m_states;
    this->m_rules = a.m_rules;

}



bool nfa_automaton::isInLanguage(const std::string &word) const {

    return isInLanguage(word, m_startStates);
}



bool nfa_automaton::isInLanguage(const std::string& word, std::vector<unsigned int> startState)const
{
    unsigned int pos = 0;
    while(pos < word.size())
    {
        if(startState.empty())          //If startStates is empty, it means there's nothing further to go, instead of
            return false;               //passing an empty vector until the whole word has been read just to see its empty
                                        //anyway break!
        if(!isInStates(startState))
            throw "unkown state while trying to resolve";


        getStates(startState, word[pos]).swap(startState);//check 'pos' letter
        pos++;
    }

    for (int i = 0; i < startState.size(); i++)
        if (isEndState(startState[i]))
            return true;                        //If there is at least one state which is a end state return true
    return false;                               //otherwise return false

}




const std::vector<unsigned int>& nfa_automaton::getStartStates() const
{
    return m_startStates;
}

void nfa_automaton::addStartState(const unsigned int& state)
{
    if(state >= m_states)
        throw "unknown state to be marked as endState";


    addWithoutDuplicate(m_startStates, state);
}




bool nfa_automaton::isEndState(const unsigned int& state) const
{
    if(state >= m_states)
        throw "unknown state";
    return isElementInVector(m_endStates,state);
}



bool nfa_automaton::isStartState(const unsigned int& state) const {

    if(state >= m_states)
        throw "unknown state";
    return isElementInVector(m_startStates, state);
}



const unsigned int& nfa_automaton::getStateCount() const {
    return m_states;
}


const std::vector<unsigned int>& nfa_automaton::getEndStates() const
{
    return m_endStates;
}


void nfa_automaton::resetEndState() {
    m_endStates.clear();
}

void nfa_automaton::resetStartState() {
    m_startStates.clear();
}


std::vector<unsigned int> nfa_automaton::findReachableStates(std::vector<unsigned int> startState) const
{
    std::vector<unsigned int> notCheckedYet = startState;
    std::vector<unsigned int> beforeVector;
    do {
        beforeVector = startState;
        for (int i = 0; i < m_alphabet.size(); i++)
            addVectorWithoutDuplicate(startState, getStates(notCheckedYet, m_alphabet[i]));

        notCheckedYet = difference(startState, beforeVector);
    }while(!notCheckedYet.empty());

    return startState;
}


std::vector<unsigned int> nfa_automaton::findUnreachableStates() const
{
    std::vector<unsigned int> unreachable;
    std::vector<unsigned int> reachable;
    findReachableStates(m_startStates).swap(reachable); //Find all reachable states first, all the other states aren't reachable

    for(unsigned int i = 0; i < m_states; i++)
        if(!isElementInVector(reachable, i))
            unreachable.push_back(i);

    return unreachable;

}



void nfa_automaton::removeState(const unsigned int& state)
{
    for(int i = 0; i < m_rules.size(); i++) {

        if (i >= state * m_alphabet.size() && i < (state + 1) * m_alphabet.size())  //no need to check since we are going to remove this
            continue;


        for (int rule = 0; rule < m_rules[i].size(); rule++) {


            if (m_rules[i][rule] == state) {
                m_rules[i].erase(m_rules[i].begin() + rule);
                rule--;//since the size of shrinks, we want to redo m_rules[i][rule]
                continue;
            }
            if (m_rules[i][rule] > state)
                m_rules[i][rule]--;
        }
    }

    m_rules.erase(m_rules.begin()+state*m_alphabet.size(), m_rules.begin()+(state+1)*m_alphabet.size());
    m_states--;



    for(int i = 0; i < m_endStates.size(); i++) {
        if (m_endStates[i] == state)
            m_endStates.erase(m_endStates.begin() + i);
        if(m_endStates[i] > state)
            m_endStates[i]--;
    }

    for(int i = 0; i < m_startStates.size(); i++) {
        if (m_startStates[i] == state)
            m_startStates.erase(m_startStates.begin() + i);
        if(m_startStates[i] > state)
            m_startStates[i]--;
    }

}