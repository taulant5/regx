#include "../header/epsilon_NFA_Automaton.hpp"


e_nfa_automaton::e_nfa_automaton(const e_nfa_automaton& a)
    {
        m_alphabet = a.m_alphabet;
        m_states = a.m_states;
        m_rules = a.m_rules;
        m_epsilonRules = a.m_epsilonRules;
        m_startStates = a.m_startStates;
        m_endStates = a.m_endStates;
    }


    e_nfa_automaton::e_nfa_automaton(const std::string& alphabet, const unsigned int& states):m_states(states)
    {
        //make sure there is one letter only in the alphabet, if there's a duplicate it simply won't be added
        for(unsigned int pos = 0; pos < alphabet.length(); pos++)
        {
            bool alreadyExists = false;
            for(int i = 0; i < m_alphabet.size(); i++)
            {
                if(alphabet[pos] == m_alphabet[i])
                {
                    alreadyExists = true;
                    break;
                }

            }

            if(!alreadyExists)
                m_alphabet.push_back(alphabet[pos]);
        }
        m_rules.resize(m_alphabet.size()*m_states);
        m_epsilonRules.resize(m_states);
    }


    e_nfa_automaton& e_nfa_automaton::Concatenate(const e_nfa_automaton& a)
    {
        if(&a == this)
            throw "cannot concatenate with itself";

        if(a.m_alphabet != m_alphabet)
            throw "not using the same alphabet, or the same order";


        unsigned int oldStates = m_states;
        addStates(a.m_states);


        for(int i = 0; i < m_endStates.size(); i++)
            for(int y = 0; y < a.m_startStates.size(); y++)
                epsilonRule(m_endStates[i], a.m_startStates[y]+oldStates);//moving all the new States to the back
                //and add an epsilon Rule from the old nea endstates to the startstates of the new nea.


        for(int newStates = 0; newStates < a.m_states; newStates++)
            for(int alphabetPos = 0; alphabetPos < m_alphabet.size(); alphabetPos++)
                for(int ruleCount = 0; ruleCount < a.m_rules[newStates*m_alphabet.size() + alphabetPos].size(); ruleCount++)
                    setRule(automatonInput(oldStates+newStates, m_alphabet[alphabetPos]), a.m_rules[newStates*a.m_alphabet.size() + alphabetPos][ruleCount] + oldStates);
                    //move the old rules to the nea


        for(int i = 0; i < a.m_states; i++)
            for(int y = 0; y < a.m_epsilonRules[i].size(); y++)
                epsilonRule(i + oldStates, a.m_epsilonRules[i][y] + oldStates);

        m_endStates.clear();
        for(int i = 0; i < a.m_endStates.size(); i++)
            addEndState(a.m_endStates[i] + oldStates);

        return *this;
    }

    e_nfa_automaton& e_nfa_automaton::Union(const e_nfa_automaton& a)
    {
        if(&a == this)
            throw "cannot generate union with itself";
        if(a.m_alphabet != m_alphabet)
            throw "not using the same alphabet, or the same order";

        unsigned int oldStatesOfOrigin = m_states;

        addStates(2 + a.m_states);



        for(int i = 0; i < m_startStates.size(); i++)
            epsilonRule(oldStatesOfOrigin, m_startStates[i]);// make a new startElement and make it point to the others with epsilonRule

        m_startStates.clear();
        addStartState(oldStatesOfOrigin);


        for(int i = 0; i < m_endStates.size(); i++)
            epsilonRule(m_endStates[i], m_states - 1);

        m_endStates.clear();
        addEndState(m_states - 1);


        for(int i = 0; i < a.m_startStates.size(); i++)
            epsilonRule(oldStatesOfOrigin, a.m_startStates[i] + oldStatesOfOrigin + 1);

        for(int i = 0; i < a.m_endStates.size(); i++)
            epsilonRule(a.m_endStates[i] + oldStatesOfOrigin + 1, m_states - 1);



        for(int newStates = 0; newStates < a.m_states; newStates++)
            for(int alphabetPos = 0; alphabetPos < m_alphabet.size(); alphabetPos++)
                for(int ruleCount = 0; ruleCount < a.m_rules[newStates*a.m_alphabet.size() + alphabetPos].size(); ruleCount++)
                    setRule(automatonInput(oldStatesOfOrigin + 1 + newStates, a.m_alphabet[alphabetPos]), a.m_rules[newStates*a.m_alphabet.size() + alphabetPos][ruleCount] + oldStatesOfOrigin + 1);



        for(int i = 0; i < a.m_states; i++)
            for(int y = 0; y < a.m_epsilonRules[i].size(); y++)
                epsilonRule(i + oldStatesOfOrigin + 1, a.m_epsilonRules[i][y] + oldStatesOfOrigin + 1);

        return *this;


    }






e_nfa_automaton& e_nfa_automaton::Star()
{
        unsigned int oldStates = m_states;
        addStates(2);

        for(int i = 0; i < m_startStates.size(); i++) //Make the new startpoint point to the startstates
            epsilonRule(oldStates,m_startStates[i]);

        epsilonRule(oldStates, oldStates + 1);          //As well as the endstate

        for(int i = 0; i < m_endStates.size(); i++)
            for(int y = 0; y < m_startStates.size(); y++)
                epsilonRule(m_endStates[i], m_startStates[y]);

        for(int i = 0; i < m_endStates.size(); i++)
            epsilonRule(m_endStates[i], oldStates + 1);

        m_endStates.clear();
        m_startStates.clear();
        addStartState(oldStates);
        addEndState(oldStates+1);
        return *this;
}



    void e_nfa_automaton::addStates(const unsigned int& i)
    {
        m_states += i;
        m_rules.resize(m_alphabet.size()*m_states);
        m_epsilonRules.resize(m_states);
    }

    void e_nfa_automaton::setRule(const automatonInput& input, const unsigned int& outState)
    {
            if(input.first >= m_states)
                throw "Unknown startState given as parameter while setting Rule";
            if(outState >= m_states)
                throw "Unknown endState given as parameter while setting Rule";



            for(int pos = 0; pos < m_alphabet.size(); pos++)
            {
                if(m_alphabet[pos] == input.second)
                {
                    for(int i = 0; i < m_rules[input.first*m_alphabet.size() + pos].size(); i++)
                    {
                        if(m_rules[input.first*m_alphabet.size() + pos][i] == outState)                 //Since we can have multiple outcomes we first check whether there
                            return;                                                                     //already is
                    }
                    m_rules[input.first*m_alphabet.size() + pos].push_back(outState);

                    return;
                }
            }

            throw "unknown character";                                              //If you came this far, it's either and unkown state
            //or unknown character but the state has been checked earlier
        }

    void e_nfa_automaton::epsilonRule(const unsigned int& input, const unsigned int& output)
    {
        if(input >= m_states)
            throw "unkown input state while trying to add epsilon Rule";
        if(output >= m_states)
            throw "unkown output state while trying to add epsilon Rule";

         addWithoutDuplicate(m_epsilonRules[input], output);
    }



    void e_nfa_automaton::addEndState(const unsigned int& state) {

        if(state >= m_states)
            throw "unknown state to be marked as endState";

        addWithoutDuplicate(m_endStates, state);
    }

    void e_nfa_automaton::addStartState(const unsigned int& state)
    {
        if(state >= m_states)
            throw "unknown state to be marked as endState";

        addWithoutDuplicate(m_startStates,state);
    }

    bool e_nfa_automaton::isEndState(const unsigned int& state) const
    {
        if(state >= m_states)
            throw "unknown state";

        return isElementInVector(m_endStates,state);
    }


    bool e_nfa_automaton::isEndStates(const std::vector<unsigned int>&input ) const
    {
        for(int i = 0; i < input.size();i++)
            if(isEndState(input[i]))
                return true;

        return false;
    }

    bool e_nfa_automaton::isStartState(const unsigned int& state)const
    {
        if(state >= m_states)
            throw "unknown state";

        return isElementInVector(m_startStates,state);
    }



    std::vector<unsigned int> e_nfa_automaton::findEpsilonRules(const unsigned int& input) const
    {
        std::vector<unsigned int> output;
        unsigned long int oldSize = 0;
        unsigned long int newSize = output.size();
        addVectorWithoutDuplicate(output, m_epsilonRules[input]);
        while(oldSize < output.size())
        {
            oldSize = output.size();
            for(unsigned long int i = newSize; i < output.size(); i++)
                addVectorWithoutDuplicate(output, m_epsilonRules[output[i]]);
            newSize = output.size();
        }
        return output;
    }



    nfa_automaton e_nfa_automaton::generateNEA() const
    {
        nfa_automaton temp(m_alphabet, m_states);
        for(unsigned int states = 0; states < m_states; states++) {



            for (unsigned int letter = 0; letter < m_alphabet.size(); letter++)
                for (int outcome = 0; outcome < m_rules[states * m_alphabet.size() + letter].size(); outcome++)
                    temp.setRule(automatonInput(states, m_alphabet[letter]), m_rules[states * m_alphabet.size() + letter][outcome]);


            std::vector<unsigned int> possibleStates;
            findEpsilonRules(states).swap(possibleStates);
            for(int epsilonStates = 0; epsilonStates < possibleStates.size(); epsilonStates++)
                for (unsigned int letter = 0; letter < m_alphabet.size(); letter++)
                    for (int outcome = 0; outcome < m_rules[possibleStates[epsilonStates] * m_alphabet.size() + letter].size(); outcome++)
                        temp.setRule(automatonInput(states, m_alphabet[letter]), m_rules[possibleStates[epsilonStates] * m_alphabet.size() + letter][outcome]);

            if(isEndStates(possibleStates) || isEndState(states))
                temp.addEndState(states);
            if(isStartState(states))
                temp.addStartState(states);

        }

        return temp;
    }
