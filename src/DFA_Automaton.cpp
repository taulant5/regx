#include "../header/DFA_Automaton.hpp"




dfa_automaton::dfa_automaton(const std::string& alphabet, const unsigned int& states):m_states(states)
{


    //make sure there is one letter only in the alphabet, if there's a duplicate it simply won't be added
    for(unsigned int pos = 0; pos < alphabet.length(); pos++)
    {
        bool alreadyExists = false;
        for(int i = 0; i < m_alphabet.size(); i++)
        {
            if(alphabet[pos] == m_alphabet[i])
            {
                alreadyExists = true;
                break;
            }

        }

        if(!alreadyExists)
            m_alphabet.push_back(alphabet[pos]);
    }



    //to be fully defined the automata has to have rules for every possible combination.
    //rules/overPasses are saved in m_rules and are being accessed by [state*alphabetSize + letter]
    m_rules.resize(m_alphabet.size()*m_states, m_states);

}


dfa_automaton::dfa_automaton(const dfa_automaton& a) {
    this->m_alphabet = a.m_alphabet;
    this->m_states = a.m_states;
    this->m_endStates = a.m_endStates;
    this->m_rules = a.m_rules;

}


void dfa_automaton::setRule(const automatonInput& input, const unsigned int& endState){
    if(input.first >= m_states)
        throw "Unknown startState given as parameter while setting Rule";
    if(endState >= m_states)
        throw "Unknown endState given as parameter while setting Rule";



    for(int pos = 0; pos < m_alphabet.size(); pos++)
    {
        if(m_alphabet[pos] == input.second)
        {
            m_rules[input.first*m_alphabet.size() + pos] = endState;         //set given state and letter to output a
            return;                                                         //specific rule
        }
    }

    throw "unknown character";                                              //If you came this far, it's either and unkown state
                                                                            //or unknown character but the state has been checked earlier
}


void dfa_automaton::addEndState(const unsigned int& state){
    if(state >= m_states)
        throw "unknown state to be marked as endState";

    addWithoutDuplicate(m_endStates, state);
}


const unsigned int& dfa_automaton::getState(const automatonInput& input) const{
    if(input.first >= m_states)
        throw "unknown state, while getting State";



    for(int pos = 0; pos < m_alphabet.size(); pos++)
    {
        if(m_alphabet[pos] == input.second)
        {
            return m_rules[input.first*m_alphabet.size() + pos];
        }
    }

    throw "unknown character";

}


bool dfa_automaton::isDefined() const{
    //We check every rule we have, if there is just one rule which is not defined, we return false
    for(int i = 0; i < m_rules.size();i++)
        if (m_rules[i] >= m_states)
            return false;

    return true;
}


bool dfa_automaton::isInLanguage(const std::string& word, unsigned int startState) const{

    unsigned int pos = 0;
    while(pos < word.size())
    {
        if(startState >= m_states)              //If you haven't checked whether the automata is fully defined or not
            throw "undefinded state or rule";//you can't know whether startState was users input or be the automata itself

        startState = getState(automatonInput(startState, word[pos]));
        pos++;
    }


    return isEndState(startState);         //check whether its inside endStates or not



}


bool dfa_automaton::isEndState(const unsigned int& state) const
{
    if(state >= m_states)
        throw "unknown state";
    return isElementInVector(m_endStates, state);
}


void dfa_automaton::merge(const statePair& mergeableStates)
{
    if(mergeableStates.first > m_states || mergeableStates.second >= m_states)
        throw "unkown states when trying to merge or tried to merge startstate";



    unsigned int mergeFirst = mergeableStates.first;
    unsigned int mergeSecond = mergeableStates.second;
    if(mergeSecond < mergeFirst)
    {
        unsigned int temp;
        temp = mergeSecond;
        mergeSecond = mergeFirst;
        mergeFirst = temp;
    }


    //Make every rule point instead of mergeSecond to mergeFirst, and every rule pointing on to higher than mergeSecond point one below
    for(int i = 0; i < m_states*m_alphabet.size(); i++) {
        if (i >= mergeSecond * m_alphabet.size() && i < (mergeSecond + 1) * m_alphabet.size())
            continue;
        if(m_rules[i] == mergeSecond)
            m_rules[i] = mergeFirst;
        else if(m_rules[i] > mergeSecond)
            m_rules[i]--;
    }

    m_rules.erase(m_rules.begin() + mergeSecond*m_alphabet.size(), m_rules.begin() + (mergeSecond+1)*m_alphabet.size());
    m_states--;

    //Fix endstates if any of them is higher than mergeSecond or points to it
    for(int i = 0 ; i < m_endStates.size(); i++)
    {
        if(m_endStates[i] == mergeSecond)
            m_endStates.erase(m_endStates.begin() + i);
        else if (m_endStates[i] > mergeSecond) {
            m_endStates[i]--;
            continue;
        }
    }



}


void dfa_automaton::makeMinimal()
{
    if(!isDefined())
        throw "Cannot make an minimal automata without it being fully defined!";

    std::vector<tuple<unsigned int, unsigned int>>pairs;
    findMergable().swap(pairs);
    while(!pairs.empty())
    {
        merge(statePair(pairs.back().first, pairs.back().second));
        findMergable().swap(pairs);
    }
}


std::vector<tuple<unsigned int, unsigned int>> dfa_automaton::findMergable() const
{
    if(!isDefined())
        throw "cant find mergeable without the automata being fully defined";


    unsigned int possibeCombinations = (m_states*(m_states - 1))/2;   //using Gauß' idea, to represent a rectangle with all possibilities

    bool* marked = new bool[possibeCombinations];

    //initial mark
    for(unsigned int y = m_states - 1; y >= 1; y--)
        for(unsigned int x = 0; x < y ; x++) {
            unsigned int calculatedIndex=(m_states - 1 - y)*(m_states - 1) -((m_states - 2 - y)*(m_states - 1 - y))/2 + x;
            marked[calculatedIndex] = (isEndState(y) ^ isEndState(x));
        }
    //can be partly negative/underflow but mulitplying by 0 makes it unsigned again
    //using gauß algorithm to count only half of the square given. draw a pyramid and see for yourself


    bool changed;
    do
    {
        changed = false;
        for(unsigned int y = m_states - 1; y >= 1; y--)
            for(unsigned int x = 0; x < y ; x++) {
                unsigned int oldCalculatedIndex = (m_states - 1 - y) * (m_states - 1) - ((m_states - 2 - y) * (m_states - 1 - y)) / 2 + x;
                if (!marked[oldCalculatedIndex]) //If its marked you can skip it
                    for (unsigned int i = 0; i < m_alphabet.size(); i++) {

                            //Find the those, who point to an marked one when adding an letter
                            unsigned int newStateY = m_rules[y * m_alphabet.size() + i]; //The state with added letter to check for the next state
                            unsigned int newStateX = m_rules[x * m_alphabet.size() + i];

                            if (newStateX == newStateY)
                                continue;

                            if (newStateY < newStateX) {  //It's not possible for X to be greater than Y, swap
                                unsigned int temp = newStateY;
                                newStateY = newStateX;
                                newStateX = temp;
                            }

                            unsigned int newCalculatedIndex = (m_states - 1 - newStateY) * (m_states - 1) -
                                                              ((m_states - 2 - newStateY) * (m_states - 1 - newStateY)) /
                                                              2 +
                                                              newStateX;

                            if (marked[newCalculatedIndex]) {
                                marked[oldCalculatedIndex] = true;
                                changed = true;
                            }

                    }
            }
    }while(changed);



    //make new automaton, same number of letters but for each false boolean one state less.
    std::vector<tuple<unsigned int, unsigned int>> mergeableStates;

    for(unsigned int y = m_states - 1; y >= 1; y--)
        for(unsigned int x = 0; x < y ; x++){

            unsigned int calculatedIndex=(m_states - 1 - y)*(m_states - 1) -((m_states - 2 - y)*(m_states - 1 - y))/2 + x;

            if(!marked[calculatedIndex])
                mergeableStates.emplace_back(x,y);
        }




    delete[] marked;
    return mergeableStates;

}


void dfa_automaton::makeComplement() {
    std::vector<unsigned int> temp;
    for(unsigned int i = 0; i < m_states; i++)
        if(!isEndState(i))
            temp.push_back(i);

    m_endStates.swap(temp);
}

const unsigned int& dfa_automaton::getStateCount() const{
    return m_states;
}