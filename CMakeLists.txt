cmake_minimum_required(VERSION 3.13)
project(RegX)

set(CMAKE_CXX_STANDARD 17)

add_executable(RegX src/main.cpp header/DFA_Automaton.hpp src/DFA_Automaton.cpp header/AutomatonHelper.hpp header/NFA_Automaton.hpp src/NFA_Automaton.cpp header/epsilon_NFA_Automaton.hpp src/epsilon_NFA_Automaton.cpp src/AutomatonHelper.cpp header/RegX.hpp)